# Conteneur

- identifiable
  - code iata
    - truc a 3 lettre
    - ville
    - pays

- definissable
  - nom
  - description
  - mots clefs
  - type
  - poids
  - volume
  - dimensions
  - fragile
  - recommandations
  - images

- localisable
  - emplacement (conteneur)
  - recommandations


