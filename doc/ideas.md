# dashboard

- alertes
  - perissable
  - quantifiable
  - reservoir
  - pret
  - localisation
- stats
- actions
- navigation

# recherche
- ai
  - conteneurs
  - items
  - evenements

# evenements

  - localisable

    - entree
      - date
      - dernier emplacement?
      - qui?

    - sortie
      - date
      - dernier emplacement?
      - qui?

  - quantifiable
    - quantite minimum depassee?
    - quantite maximum depassee?
    - qte zero?

  - perissable
    - date
    - dlc depassee?
    - dluo depassee?
    - dlalerte depasee?

  - documentable
    - ajout document
      - date
      - document
      - qui?

