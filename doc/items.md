# Objet

- identifiable
  - code

- definissable
  - nom
  - description
  - mots clefs
  - type
  - couleur
  - poids
  - volume
  - dimensions
  - fragile
  - recommandations
  - images

- perissable
  - dlc
  - dluo
  - dlalerte

- commandable
  - lien achat panier
  - lien achat direct
  - prixttcavecfdp
  - poids
  - delaimoyenreception
  - delaivendeur

- quantifiable
  - qte
  - alerte y/n
  - qtealerte

- reservoir
  - contenance
  - restant
  - unitee
  - alerte y/n
  - qtealerte

- localisable
  - emplacement (conteneur)
  - recommandations

- documentable
  - documents

- collectionnable
  - collection

- pretable
  - a qui
  - datepret
  - alertedateretour y/n
  - retourdatealerte

- serialisable
  - numero de serie

- unique
  - objet unique y/n
