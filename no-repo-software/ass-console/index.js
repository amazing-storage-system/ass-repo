const db = require('../ass-db');
const qrcode = require('qrcode-terminal');
const chalk = require('chalk');
const ctx = new chalk.constructor({enabled: true});
const log = console.log;

const CFonts = require('cfonts');

const prettyFont = CFonts.render('CDG', {
	font: 'block',              // define the font face
	align: 'center',              // define text alignment
	colors: ['red'],         // define all colors
	background: 'transparent',  // define the background color, you can also use `backgroundColor` here as key
	letterSpacing: 1,           // define letter spacing
	lineHeight: 1,              // define the line height
	space: true,                // define if the output text should have empty lines on top and on the bottom
	maxLength: '3',             // define how many character can be on one line
});

process.stdout.write('\033c');

qrcode.generate('box://CDG');

log(prettyFont.string);